import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import project.Comment;
import project.Model;
import project.ProjectFactory;
import project.ProjectPackage;
import project.Requirement;
import project.RequirementGroup;
import project.Version;

public class XMIParser {
	private final Model model;
	private String messagePrefix = "";

	public XMIParser(String file) throws Exception {
		try {
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

			ProjectPackage.eINSTANCE.eClass();

			ProjectFactory factory = ProjectFactory.eINSTANCE;

			ResourceSet resSet = new ResourceSetImpl();

			Resource resource = resSet.getResource(URI.createURI(file), true);

			resource.load(Collections.EMPTY_MAP);

			EObject root = resource.getContents().get(0);

			this.model = (Model) root;
		} catch (Exception e) {
			throw new Exception("Input file cannot be parsed.");
		}
	}

	public void execute() throws Exception {
		validateModel();
	}

	private void validateModel() throws Exception {
		// Validate Title
		this.messagePrefix = "Model Title";
		validateString(model.getTitle());

		for (RequirementGroup reqGroup : this.model.getGroups()) {
			validateRequirementGroup(reqGroup);
		}

		System.out.println("\nAll requirements groups were valid.");
	}

	private void validateRequirementGroup(RequirementGroup reqGroup) throws Exception {
		this.messagePrefix = "Requirement Group \"" + reqGroup.getName() + "\"";

		// Validate ID
		validateID(reqGroup.getId());

		this.messagePrefix += "Description";
		// Validate Description
		validateString(reqGroup.getDescription());

		// Validate Requirements
		for (Requirement req : reqGroup.getRequirements()) {
			validateRequirement(req);
		}

		System.out.println("\nRequirement Group \"" + reqGroup.getName() + "\" requirements were valid.");
	}

	private void validateRequirement(Requirement req) throws Exception {
		// Validate Title
		this.messagePrefix = "Requirement Title";
		validateString(req.getTitle());

		String prefix = "Requirement \"" + req.getTitle() + "\"";
		this.messagePrefix = prefix;

		// Validate ID
		validateID(req.getId());

		// Validate Description
		this.messagePrefix = prefix + " Description";
		validateString(req.getDescription());

		// Validate Author
		this.messagePrefix = prefix + " Author";
		validateString(req.getAuthor());

		// Validate the Version
		this.messagePrefix = prefix + " Version";
		validateVersion(req.getVersion());

		// For comment validation
		Date requirementDate = req.getCreated();

		// Validate comments
		for (Comment comment : req.getComments()) {
			this.messagePrefix = prefix + " Comment";
			validateComment(comment);

			// Validates Date
			Date commentDate = comment.getCreated();

			if (requirementDate.after(commentDate)) {
				throw new Exception(this.messagePrefix + " Date is not correct");
			}
		}
	}

	private void validateVersion(Version version) throws Exception {
		if (version.getMajor() < 0) {
			throw new Exception(this.messagePrefix + " Major cannot be negative.");
		}

		if (version.getMinor() < 0) {
			throw new Exception(this.messagePrefix + " Minor cannot be negative.");
		}

		if (version.getService() < 0) {
			throw new Exception(this.messagePrefix + " Service cannot be negative.");
		}
	}

	private void validateComment(Comment comment) throws Exception {
		String prefix = this.messagePrefix;
		this.messagePrefix = prefix;

		// Validate Author
		this.messagePrefix = prefix + " Author";
		validateString(comment.getAuthor());

		// Validate Author
		this.messagePrefix = prefix + " Subject";
		validateString(comment.getSubject());

		// Validate Author
		this.messagePrefix = prefix + " Body";
		validateString(comment.getBody());

		// Recursively validate children
		for (Comment child : comment.getChildren()) {
			validateComment(child);
		}
	}

	private void validateID(String id) throws Exception {
		this.messagePrefix += " ID";
		validateString(id);
		
		// If ID is a number, it cannot be negative
		long idAsNumber = 0;

		try {
			idAsNumber = Long.parseLong(id);
		} catch (Exception e) {

		}

		if (idAsNumber < 0) {
			throw new Exception(this.messagePrefix + " cannot be a negative number.");
		}

	}

	private void validateString(String string) throws Exception {
		if (string == null || string.trim().isEmpty()) {
			throw new Exception(this.messagePrefix + " cannot be empty.");
		}
	}

}
