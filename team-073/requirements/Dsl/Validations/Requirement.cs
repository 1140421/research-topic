﻿using Microsoft.VisualStudio.Modeling;
using Microsoft.VisualStudio.Modeling.Validation;

namespace edom.requirements
{

    // Allow validation methods in this class:
    [ValidationState(ValidationState.Enabled)]
    // In this DSL, ParentsHaveChildren is a domain relationship
    // from Person to Person:
    public partial class Requirement
    {
        // Identify the method as a validation method:
        [ValidationMethod
        ( // Specify which events cause the method to be invoked:
          ValidationCategories.Open // On file load.
        | ValidationCategories.Save // On save to file.
        | ValidationCategories.Menu // On user menu command.
        )]
        // This method is applied to each instance of the
        // type (and its subtypes) in a model:
        private void ValidateRequirement(ValidationContext context)
        {
            // In this DSL, the role names of this relationship
            // are "Child" and "Parent":

            if (string.IsNullOrEmpty(this.description))
            {
                context.LogError("Desciption mandatory", "FAB001RequirementError");
            }
        }
    }
}