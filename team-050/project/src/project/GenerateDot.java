package project;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class GenerateDot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		loadModel();
	}

	public static void loadModel() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("req", new XMIResourceFactoryImpl());

		// Initialize the model
		ProjectPackage.eINSTANCE.eClass();

		// Retrieve the default factory singleton
		ProjectFactory factory = ProjectFactory.eINSTANCE;

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		Resource resource = resSet.getResource(URI.createURI("instances/generatedModel.req"), true);

		// now load the content.
		PrintWriter writer = null;
		try {
			resource.load(Collections.EMPTY_MAP);

			EObject root = resource.getContents().get(0);
			Model myProject = (Model) root;

			System.out.println(root.toString())
			;
			// -----
			// Generate a plantuml file that represents the mind map diagram
			FileWriter w = new FileWriter("instances/generatedModel.puml");
			writer = new PrintWriter(w);

			writer.println("@startuml");

			writer.println("object Model { \r\n title = \"" + myProject.getTitle() +"\" \r\n}");

			// For the Groups
			for (RequirementGroup reqGroup : myProject.getGroups()) {
				writer.println(
						"object " + reqGroup.getId() + "{ \r\n" + " name = \"" + reqGroup.getName() + "\" " + "\r\n}");

				writer.println("Model *-- " + reqGroup.getId() + "");

			}

			for (RequirementGroup reqGroup : myProject.getGroups()) {
				// For the Requirements
				for (Requirement req : reqGroup.getRequirements()) {
					writer.println("object " + req.getId() + "{ \r\n" + "name = \"" + req.getTitle() + "\" "
							+ "\r\n title =\"" + req.getTitle() + "\" " + "\r\n description =\"" + req.getDescription()
							+ "\" " + "\r\n author =\"" + req.getAuthor() + "\" " + "\r\n priority =\""
							+ req.getPriority() + "\" " + "\r\n resolution =\"" + req.getResolution() + "\" "
							+ "\r\n state =\"" + req.getState() + "\" \r\n}");
					
					writer.println(reqGroup.getId() + " *-- " + req.getId() + "");
				}
			}

			// resource.save(Collections.EMPTY_MAP);
			writer.println("@enduml");
			writer.close();
			
			System.out.println("Everything worked fine.");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			writer.close();
		}
	}
}
