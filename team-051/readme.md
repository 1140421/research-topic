# EDOM - Project Research Topic

# Integrating DSLs and Java in XText

## Concern and approach

O tópico em questão é “Models are Code and Code is Modeling”. Na sua essência e como o nome do tópico indica irá ser uma **relação mútua** entre ambas as partes. O código depende da modulação bem como a modulação depende do código, o que leva a que alterações em qualquer uma das partes leve a consequentes alterações na outra de modo a manter a coerência.

De modo a explicar este tópico de uma melhor forma iremos abordá-lo através da integração de DSLs e Java em XText. As DSLs vêm trazer uma solução ao problema acima apresentado. Estas permitem uma modulação gerada especificamente com um problema em mente, sendo depois apenas usadas neste domínio específico. Estas podem ser de diferentes tipos, como por exemplo, o EMF que se trata de um modelo ou linguagens textuais como o XText. Neste caso vamos, como referido acima, abordar em particular o seu uso através de XText.

Esta framework oferece uma linguagem simples para a criação de DSLs bem como facilita o desenvolvimento através da sua boa integração com Java, o que permite a geração do código necessário de maneira bastantes simples, indo de encontro ao problema mencionado inicialmente.


## Applying the approach
Inicalmente será feita uma breve abordagem de como integrar uma DSL com código Java. Iremos demostrar como referenciar elementos Java já existentes na nossa linguagem. De seguida será utilizado Xbase para refenciar tipos genéricos seguindo-se o mapeamento de conceitos de DSL para conceitos de Java. Para finalizar, tipos e conceitos Java serão usados como expressões Xbase de forma a serem executadas.
Para referenciar uma devida DSL basta usar a correta sintaxe XTexr referenciando o ficheiro "grammar org.xtext.mydsl.MyDsl with org.eclipse.xtext.xbase.Xtype".


É habitual que ao desenvolver a nossa própria DSL tenhamos a necessidade de referenciar conceitos já existentes noutras linguagens, ou mesmo conceitos do próprio ambiente de execução (por estarmos a falar de integração com Java, o ambiente de execução que passaremos a ter em conta é a Java Virtual Machine). A JVM contém modelos Ecore que possibilitam a sua inclusão nas linguagens desenvolidas por nós. É possível aceder a qualquer elemento da JVM, desde classes, interfaces, os seus campos e métodos. Tudo isto é possivel por simplesmente importar o modelo JavaVMTypes Ecore: 
import "http://www.eclipse.org/xtext/common/JavaVMTypes" as jvmTypes
Depois disso podemos efectuar uma referência cruzada entre um elemento da nossa linguagem e um elemento importado.

Como o objetivo da demonstração é gerar código na linguagem JAVA, e sendo a própria linguagem orientada a objetos, xtext fornece suporte para transformações de entidades para classes de java.
![Entidades](https://i.ibb.co/QHRhsfL/1111.png)

É possível usar classes em vez de tipos da própria linguagem ou até misturar tipos, por exemplos listas, com entidades. Como é também visível no exemplo, também é possível gerar entidades que herdam propriedades de outras.
Na imagem seguinte é possível observar uma base para a geração de uma classe JAVA, com os devidos imports de packages, nome da entidade e possíveis atributos.

![Estrutura base para criação de classe java](https://i.ibb.co/QCpHRmQ/222.png)

## Comparision with Acceleo

Comparando a abordagem discutida neste projeto, o XBase para geração de texto (código), com a abordagem lecionada na disciplina de Engenharia de Domínio, a utilização de Acceleo para geração de código.

A criação e definição de instâncias utilizando a gramática gerada através de XText torna a integração com o Java muito mais eficaz, sendo esta uma das melhores linguagens de programação orientada a objetos. Pode-se tratar o XBase com uma ampliação do anterior, sendo que este, proporciona o mapeamento dos artefatos gerados pelo modelo ECore, gerando código Java executável. Das principais vantagens associadas à utilização deste invés da ferramenta Acceleo, é a sua **velocidade de transformação** do modelo em código, sendo estes tópicos, **performance e escalabilidade**, os principais fatores que preocupam mais os desenvolvedores da tecnologia. Com base em estudos previamente realizados, pode-se concluir que o XText, variante do XTend, possui **uma maior longevidade de vida**, sendo que prevê-se uma maior utilização desta ferramenta sobre a sua homóloga, Acceleo.

## References

[Comparision about model-to-text tools](http://research.cs.queensu.ca/TechReports/Reports/2015-627.pdf)

[Combining model and code](https://www.infoq.com/articles/combining-model-and-code)

[Model-driven engineering](https://www.sciencedirect.com/science/article/pii/S1477842415000408)

[Xtext: Integration with Java](https://www.eclipse.org/Xtext/documentation/305_xbase.html)

