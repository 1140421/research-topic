# EDOM - Project Research Topic

In this folder you should add **all** artifacts developed for the investigative topic of the project.

There is only one folder for this task because it is a team task and, as such, usually there will be no need to create individual folders.

**Note:** If for some reason you need to bypass these guidelines please ask for directions with your teacher and **always** state the exceptions in your commits and issues in bitbucket.

---

# Modelação sem EMF: Modelação com outras frameworks

## 1. Modelação e DSLs

As **linguagens específicas de domínio (DSLs)** são linguagens de propósito específico projetadas para resolver um conjunto específico de problemas. Alguns exemplos de destaque são: 

1. **HTML**: projetados para representar o layout de páginas da Web; 
2. **SQL**: projetado para consultar e atualizar bancos de dados; 
3. **Regex (expressões regulares)**: projetadas para localizar e extrair padrões específicos em cadeias textuais. 

A essência de **uma DSL** é que ela **torna um grande problema menor**. Sem HTML, o problema de renderizar páginas da Web com aparência mais ou menos equivalente em milhões de ecrãs conectados a diferentes computadores seria intransponível. Sem o SQL, o problema de permitir que vários utilizadores simultâneos estabeleçam, consultem e combinem grandes listas de dados seria uma tarefa de programação massiva. Sem expressões regulares, procurar substrings no texto envolveria escrever um programa complicado. 

Destaca-se aqui um padrão: transformar grandes problemas em pequenos problemas, identificar uma linguagem que expresse eficientemente essa classe de problemas e aplicar o padrão para conectar expressões na linguagem em que o problema deve ser resolvido no ambiente final. Dado que existem diariamente vários problemas de grande complexidade para resolver, uma das soluções viáveis para os resolver envolve a **modelação de DSLs**.

O **padrão DSL** tem **três componentes** principais: em **primeiro** lugar, é necessária a **criação do modelo** para representar o problema a tratar. Este pode ser uma representação textual, como nos casos citados anteriormente, ou pode ser uma representação visual sob a forma de diagrama. Em **segundo** lugar, é imposta a utilização de uma **plataforma para executar a solução** do problema em questão. No caso do HTML, esta plataforma será um navegador da Web; no caso do SQL, um banco de dados; e, no caso de uma expressão regular, um editor de texto ou ambiente de programação. Em **terceiro** e ultimo lugar, deve existir um método para **integrar a expressão da linguagem na plataforma** de modo a configurá-la para o problema em questão.

A figura 1 representa visualmente o padrão DSL descrito anteriormente.

![](./images/dslpattern.png)

*Figura 1. Padrão DSL*

---

## 2. Frameworks de Modelação

### 2.1. JetBrains MPS

### 2.1.1 Definição

O **JetBrains MPS** é uma **framework de metaprogramação**, ou seja, que escreve e/ou modifica código e dados em tempo de compilação, desenvolvida pela empresa de software JetBrains. O **MPS** é uma ferramenta desenhada para projetar **linguagens específicas de domínio (DSLs)** utilizando **edição projetiva**, o que permite aos programadores ultrapassar as barreiras dos *parsers* – analisadores léxicos que tipicamente vinculam a representação da persistência de código com a notação visual desenhada, não sendo facilmente conjugados com outros analisadores, o que impede a modularização das linguagens – e criar editores de DSLs através de tabelas e diagramas utilizando abordagens de **Model-Driven Design**.

Ao contrário das linguagens de programação caracterizadas pelas suas sintaxe e semântica rígidas, o MPS permite a criação e modificação de novas linguagens, implementando **programação orientada para a linguagem**, tratando-se assim de um ambiente para definição de linguagens e de um ambiente de desenvolvimento integrado (IDE) para estas. As desvantagens impostas por *parsers* na extensão de linguagens, referidas anteriormente, em particular as inseridas no bloqueio da modularização levou à ideia de desenvolver uma framework de **representação de código não-textual**.

Deste modo, a solução encontrada para o desenvolvimento do MPS passou por manter o código desenvolvido numa ***Abstract Syntax* Tree (AST)**, constituída por nós com propriedades, filhos e referências que descrevem o código dos programas a desenvolver. Esta abordagem eliminou a necessidade de *parsing*, o que contribui positivamente para um dos bloqueios identificados inicialmente. Assim, a framework apresenta aos programadores de forma *user-friendly* a AST e fornece as ferramentas necessárias para a **edição gráfica** da mesma, seguindo boas práticas de edição de diagramas e definindo as regras e as restrições da linguagem a desenvolver de forma visual.

### 2.1.2 Casos Práticos

### **Mbeddr**

Trata-se de um programa com recurso a um conjunto de linguagens integradas e extensíveis para **engenharia de software embedded** e de um IDE, suportando funcionalidades de implementação, testabilidade, validação e processamento. Integra-se com ferramentas de criação via linha de comandos e com servidores de integração, bem como sistemas de controlo de versões baseados em ficheiros. Tem suporte para requisitos e definição de linhas de produtos, documentação de software, implementação em C e extensões da linguagem C (C++, C#), bem como testes, simulação e validação formal.

O **Mbeddr** é baseado no JetBrains MPS devido às suas vantagens a nível de escalabilidade de programas e ferramentas sem restrições, devido à inexistência de ambiguidade de parsers, o que permite aos utilizadores do Mbeddr estenderem o seu trabalho desenvolvido sem a limitação de utilização de APIs de extensão de programas, integrando assim uma vantagem relativamente à concorrência do mercado.

A figura 2 representa a arquitetura do programa Mbeddr.

![](./images/mbeddr.png)

*Figura 2. Arquitetura do programa Mbeddr, utilizando a framework JetBrains MPS*

### **YouTrack**

O **YouTrack** trata-se de uma aplicação Web comercial, caracterizada por um conjunto de um **rastreador de bugs**, um **sistema de rastreamento de problemas** e por um software de gerenciamento de projetos desenvolvido pela JetBrains. Foca-se na pesquisa de problemas com base em consultas com preenchimento automático (autocomplete), na manipulação de problemas em ficheiros batch e na criação de workflows personalizados. É implementado com recurso ao padrão de programação orientada a linguagem e utiliza a framework MPS da JetBrains e as suas linguagens específicas de domínio.

A figura 3 representa a interface gráfica do programa YouTrack.

![](./images/youtrack.png)

*Figura 3. Dashboards do programa YouTrack, desenvolvido com a framework JetBrains MPS*

### **Realaxy Editor**

O **Realaxy Editor** consiste numa plataforma para criação, manipulação e demonstração de **objetos imobiliários** através da framework MPS.

A figura 4 representa a interface gráfica do programa Realaxy Editor.

![](./images/realaxyeditor.png)

*Figura 4. Criação de objetos imobiliários com o programa Realaxy Editor, com recurso à framework JetBrains MPS*

### 2.1.3 Comparação EMF *(Eclipse Modeling Framework)* **vs** JetBrains MPS

O **Xtext** é uma framework de software open-source para o desenvolvimento de linguagens de programação e de linguagens de domínio específicas (DSLs) baseado na **framework de metamodelação EMF**. Ao contrário da maioria dos geradores de analisadores léxicos, o Xtext gera não só o *parser*, como o modelo de classes para a *Abstract Syntaxt Tree* (AST) através da gramática criada, além de fornecer um IDE Eclipse customizado com todas as ferramentas necessárias para o desenvolvimento de DSLs. Deste modo, sendo o Xtext uma ferramenta textual, foca-se no **desenvolvimento de DSLs textuais** através dos analisadores léxicos.

Em oposição ao Xtext, o **MPS** utiliza uma abordagem de **edição projetiva**, isto é, manipula a *Abstract Syntaxt Tree* **graficamente**, não sendo necessária a geração de *parsers*, uma vez que a informação é toda salvaguardada diretamente na AST. Deste modo, o **MPS** garante a existência de **compatibilidade entre extensões**, uma vez que a ambiguidade de gramáticas não é um problema desta abordagem. Contrariamente, a abordagem utilizada pelo **Xtext** que usufrui de representações textuais de DSLs pode resultar em **problemas na conciliação entre extensões**.

---

### 2.2. Kevoree Modeling Framework (KMF)

### 2.2.1 Definição
O Kevoree Modeling Framework (KMF) começou como um projeto de pesquisa para criar uma alternativa ao Eclipse Modeling Framework (EMF). Como o EMF, o KMF é uma framework de modelagem e uma ferramenta de geração de código para criação de aplicações complexas orientados a objetos com base em modelos de dados estruturados. O KMF foi projetado especificamente para oferecer suporte ao paradigma “models@run.time”. Os modelos de runtime  de sistemas complexos geralmente possuem altos requisitos em relação ao uso da memória, desempenho em tempo de execução e segurança do encadeamento, tendo sido o KMF projetado para combater estes requisitos.

A fronteira entre os sistemas e modelos de gestão de dados em larga escala está a tornar-se cada vez menos rígida, à medida que modelos@run.time ganham progressivamente a maturidade através de mecanismos de distribuição e larga escala. Portanto, o KMF é como uma evolução da EMF, que evoluiu para uma estrutura para modelagem eficiente (estruturação), processamento e análise de dados em grande escala. Ele permite modelos com milhões de elementos, que não precisam mais caber completamente na memória principal, e suporta a distribuição de modelos em milhares de nós.

O KMF fornece um poderoso conjunto de ferramentas para que os desenvolvedores consigam modelar, estruturar e analisar dados complexos durante o design e o tempo de execução.  O foco principal do KMF é o desempenho e a escalabilidade, que muitas vezes são negligenciados em grande medida pela modelagem de frameworks.

### 2.2.3 Comparação EMF *(Eclipse Modeling Framework)* **vs** KMF *(Kevoree Modeling Framework)*
O Eclipse Modeling Framework (EMF) tornou-se rapidamente o padrão no MDSE para a criação de linguagens específicas de domínio (DSL) e ferramentas baseadas em técnicas geradoras. No entanto, o uso de ferramentas geradas por EMF em domínios como Internet of Things (IoT), Cloud Computing ou Models @ Runtime atinge várias limitações. Existem várias propriedades que as ferramentas geradas devem cumprir para serem usadas em outros domínios que não os sistemas de software baseados em desktop.

---

### 2.3. Microsoft DSL SDK (MS/DSL)

### 2.3.1 Definição
No centro do MS/DSL  está a definição de um modelo que é criado para representar conceitos numa área de negócios. É necessário construir o modelo com uma variedade de ferramentas, como uma exibição esquemática, a capacidade de gerar código e outros artefactos, comandos para transformar o modelo e a capacidade de interagir com código e outros objetos no Visual Studio. Ao desenvolver o modelo, é possível combiná-lo com outros modelos e ferramentas para formar um conjunto de ferramentas poderoso, centrado no seu desenvolvimento.

O MS/DSL permite desenvolver um modelo rapidamente na forma de uma domain-specific language (DSL). Inicia-se usando um editor especializado para definir um esquema ou a sintaxe abstrata em conjunto com uma notação gráfica. A partir dessa definição, o VMSDK gera:

	- Uma implementação de modelo com uma API fortemente tipada.

	- Um explorador baseado em árvore.

	- Um editor gráfico no qual se visualiza o modelo ou partes dele que você define.

	- Métodos de serialização que salvam seus modelos em XML legível.

	- Recursos para gerar código de programa e outros artefatos usando o modelo de texto.

Com esta ferramenta consegue-se personalizar e estender todos esses recursos. As suas extensões são integradas de maneira que se possa atualizar a definição de DSL e gerar novamente os recursos sem perder suas extensões.

### 2.3.2 Comparação EMF *(Eclipse Modeling Framework)* **vs** MS/DSL *(Microsoft DSL SDK)*
O Eclipse EMF (via Ecore) para definição de modelo de domínio é mais fácil de entender e expressivo o suficiente. 

O designer de metamodelo MS/DSL é mais utilizável que o designer de metamodelo EMF, uma vez que, MS/DSL fornece um designer visual em que ambos os modelos de domínio, incluindo classes e relacionamentos e elementos de diagrama e seus mapeamentos podem ser definidos. Contudo, o Eclipse atualmente fornece design visual apenas para definição de modelo de domínio, uma visão de árvore hierárquica, ferramentas e definições de mapeamento.

Em relação à geração de código, as ferramentas MS / DSL e o Eclipse EMF fornecem modelos baseados em modet-to-model, desenvolvendo assim geradores de código tem complexidade média. No entanto, escrevendo esses modelos de texto que geram código C # é uma tarefa tediosa, especialmente na ausência de ferramentas de suporte como o recurso Intellisense. Assim, a abordagem baseada em modelos de ferramentas MS/DSL para geração de artefactos é discutível.

Ferramentas MS/DSL fornecem reflexão somente no nível C #, a reflexão no nível do modelo está ausente. Além disso, é necessário mais apoio para combinar e reutilizar modelos.

As ferramentas MS/DSL não possuem uma técnica explícita para transformações de modelo para modelo, enquanto o Eclipse suporta linguagens de transformação de modelo para modelo, como ATL. Além disso, a abordagem da Microsoft visa gerar código a partir de modelos diretamente que é melhor opção particularmente em sistemas de larga escala, onde todos os modelos de alto e baixo nível devem ser mantidos e consistentes.

O Eclipse fornece uma estrutura de validação de modelo mais abrangente, comparando com os validadores programáticos de MS/DSL Tools.

---

## 3. Referências

1. https://msdn.microsoft.com/en-us/library/bb245773.aspx
2. https://www.jetbrains.com/mps
3. http://mbeddr.com/
4. https://www.jetbrains.com/youtrack/
5. https://realaxy.net/
6. http://modeling.kevoree.org/
7. https://www.semanticscholar.org/paper/Kevoree-Modeling-Framework-(KMF)%3A-Efficient-for-use-Fouquet-Nain/94e1db13fbb7b69e2da37ac9e35aa30819c53576
8. https://hal.inria.fr/hal-00996764/document
9. https://docs.microsoft.com/en-us/visualstudio/modeling/getting-started-with-domain-specific-languages?view=vs-2017
10. https://docs.microsoft.com/en-us/visualstudio/modeling/how-to-define-a-domain-specific-language?view=vs-2017
11. https://msdn.microsoft.com/pt-br/library/bb126581.aspx