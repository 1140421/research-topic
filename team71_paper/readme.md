# EDOM - Project Research Topic
# Continuous Integration on EMF solutions

### Lu�s Silva - 1111511
### Pedro Santos - 1120653
### Jos� Magalh�es - 1170038
### Sven Rodrigues - 1180431

## Introduction

With the current growth in Software Development, that increases the complexity of solutions as well as the demand for more features with fewer bugs, the Continuous Integration process has gotten a rise in popularity.[1]
This is the practice of merging all developer working copies to a shared mainline several times a day. It's a useful practice that keeps the solution running and its code consistent.[2]

Using Model-Driven Engineering, it is possible to look into the solution's problems in a more abstract way, offering a different perspective, covering the "Design" and "Analysis" stages of software development, as well as providing a more "maintanable" application.[1,3]

In a normal scenario, developers will run into various types of problems with the Continous Integration and in EMF solutions, these problems are amplified because of:

- The code structure;
- The inconsistencies between merging models;
- Performance issues and flaws on business workflow.

The approach taken to achieve the goal of Continuous Integration in an EMF solution is to integrate EMF validation steps into a pipeline, besides adding the basic functions of building and running unit / integration tests. For instance, in a Jenkins pipeline, there should be the normal pipeline steps such as building, running unit/integration tests by Gradle but it should also have steps for EMF validation like:

- Metamodel Validation;
- DSL Validation;
- Model Transformations (optional).

[1] https://pdfs.semanticscholar.org/b3e6/71fe7cb59ede977c778aea73b2cdd8bebd20.pdf

[2] http://ceur-ws.org/Vol-2245/commitmde_paper_1.pdf?fbclid=IwAR1FfhJ32WQRLCWK3bt7vlkGdrDhNij1XfRWJz0QAHpIhq7rA0hI0IH5-Nw

[3] "Stepwise Adoption of Continuous Delivery in Model-Driven Engineering" - Jokin Garcia & Jordi Cabot

## Step: Metamodel Validation
To apply, in a pipeline style, continuous integration in a project developed in the Eclipse model Framework, it is necessary to create a step that executes validations to the metamodel defined. This step should always run after the metamodel/ecore change, and its main goal should be assuring that the updated metamodel is valid.

### Validate the ecore file
To achieve this, first the ecore file must be validated, this means loading the OCL (Object Constrain Language) identifying all constrains for the model and match them to the elements. If all elements and constrains are matched, the ecore is valid and the genmodel file can be used to generate the code for the updated model.

### Generate source code
The second task this step should do is generate the source code for the metamodel developed. This code can be generated thought the genmodel file. It will typically generate plugins for Model, Edit, Editor and Test. The model contains all the entities, packages and factories to create instances of the model and the Test contains the templates to write testes for the model [5].

### Test the model
Finally, the step should run the unit and integration tests defined for the model.

### Conclusion
With these actions, the step assures that the metamodel is valid and can be used for the next steps of the pipeline. As already mention this step should always run when the metamodel is changed and all the tasks should have a successful feedback, otherwise the step should fail, and the pipeline should not continue.

[4] http://www.eclipsesource.com/blogs/tutorials/emf-tutorial/

[5] http://www.vogella.com/tutorials/EclipseEMF/article.html

## Step: DSL Validation

After validating the ecore model from the previous step, the main goal of this step is to generate the DSL from the ecore model and validate the code. For instance, if a Use Case EMF project model is valid, then this step should be able to generate a sample XText project for that Use Case model.

### Generate the DSL

For each model in the EMF solution, the pipeline should be able to generate its DSL by creating/updating a sample DSL project from the ecore model, build this project and execute its MWE2 generation file from command prompt.

### Validate the DSL

After generating the DSL, the pipeline should validate the DSL by checking the generated files and check if every entity from the metamodel was generated from the xtext file.

### Validate non-generated files

For non-generated files (mostly Java files), the pipeline should try to build the project again and validate these files.

### Run DSL generated files' unit and integration tests

The pipeline should validate the integrity of the generated source code by executing the unit tests (can be based off JUnit) from command prompt.

### Conclusion 

After making these actions, this will conclude this step with all the validated DSL. In a normal pipeline with no transformations, this would be one of the last steps of the EMF solution based pipeline.

.[6] http://www.informit.com/articles/article.aspx?p=1329501&seqNum=2

.[7] https://www.eclipse.org/Xtext/documentation/308_emf_integration.html

## Step: Validate Model to Model transformations

This step will only run if there are transformations in the project. The main goal of this step is to validate the model transformation of the solution. For instance, if a Use Case EMF project has model transformations for Entity Models, this step is responsable to check the validity of the model transformation.

### Run the transformation

To be able to run the transformation our pipeline would need to execute the transformation. This could be done by calling the a gradle command. If the execution returned an error code, the step would be terminated with a failed state.

### Validate the transformation and results

In the context of the given example, the pipeline should validate the created instance from the transformation.

## Step: Validate Model to Text Transformations

### The goal of validation the Model to Text transformation
The main goal of testing the Model to Text transformation, as in other tests, is to validate and verify the code added by the developers and to avoid bugs or other problems in the final result. 
An example of a Model to Text tranformation could be the transformation of an Use Case model to a full Java GWT Application. This application could then be tested using JUnit unit and integration tests to guarantee that the application does what's expected (verify) and without errors (validate).

### Run the transformation
To be able to run the transformation our pipeline would need to execute the transformation. This could be done by calling the a gradle command. If the execution returned an error code, the step would be terminated with a failed state.

### Validate the transformation
In the context of the given example, where the result of the tranformation is a Java GWT Application, our pipeline could execute JUnit unit and integration tests and publish the resulting reports to an email-list or a platform.
It is also possible to specifically test the generation by checking if the transformation result is identical to a pre-defined expected value.
