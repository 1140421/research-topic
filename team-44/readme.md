# EDOM - Project Research Topic

**Topic**: Model Transformation

**Concern**: Model to Text Transformation (M2T) with Xpand

## Índice
1. Transformação de Modelos;
2. Transformação de Modelos para Texto, utilizando Xpand;
3. Exemplo de utilização de Xpand para M2T: Requirements Model;
4. Bibliografia.

---

## 1. Transformação de Modelos

A Transformação de Modelos é uma caracteristica chave do desenvolvimento de software orientado a Modelos - Model-Driven Software Engineering (MDSE).

*”Besides models, model transformations represent the other crucial ingredient of MDSE
and allow the definition of mappings between different models.”*

A possibilidade de fazer transformações entre modelos é uma das grandes vantagens da modelação relativamente a apenas desenhar estes modelos. Tal como os modelos são especificados recorrendo a uma linguagem específica definida por uma *metamodeling language*, também as transformações são definidas com base em regras definidas em linguagens de transformação específicas.

As transformações podem ter vários objetivos como:

 - o refinamento de um modelo existente, segundo novas regras (M2M)
 - a geração de código através de um modelo (M2T)
 
Normalmente uma transformação aumenta o nível de detalhe do Modelo, tornando este cada vez mais *low-level* até atingir a fase de código executável.

Principio MDSE: **Modelos + Transformações = Software**

## 2. Transformação de Modelos para Texto, utilizando Xpand
### 2.1 Transformação de Modelos para Texto

Através de um modelo pode ser gerados vários tipos de texto:

 - Código
 - Documentação
 - Relatórios
 - Modelos serializados (XMI) - modelos em forma de texto
 
Geralmente este tipo de transformação é utilizado para a geração de código.

Esta abordagem do processo de desenvolvimento de software orientado a Modelos permite a separação de conceitos entre a modelação da aplicação e o código técnico. Com isto facilita-se processos como a manutenção, extenção e portabilidade para outros sistemas. Também facilita o desenvolvimento de protótipos, o que permite um ciclo de feedback mais rápido ao cliente.

Uma desvantagem da utilização deste tipo de transformação é a divisão da informação por várias fontes, como o Modelo e código gerado, que podem originar inconsistências.

### 2.2 *Template based Transformation*

A utilização de templates na enganharia de software é um prática comum.
Templates são fragmentos de texto que contém marcadores (*meta-markers*). Estes *meta-markers* são utilizados para representar informação de forma dinâmica, ou seja, tendo em conta informações do modelo. Para retirar estas informações ao modelo podem ser utilizadas * Declarative Query Languages*, como o OCL, ou linguagens de programação como o Java.

O *template engine* é responsável for substituir estes meta-markers por dados e produzir os ficheiros de output.

Engine:

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/m2t-template-code-gen.jpg "Template based Transformation")

Vantagens da utilização de templates:

 - Separação entre código estático e dinâmico
 - Output estruturado (template)
 - Utilização de *declarative query languages* (e.g.OCL)

### 2.3 Xpand

Xpand is a statically-typed template language featuring:

 * polymorphic template invocation
 * aspect oriented programming
 * functional extensions
 * a flexible type system abstraction
 * model transformation and validation

Suporta utilização de modelos Ecore e utiliza um ficheiro .mwe para a geração de código.

## 3. Exemplo de utilização de Xpand para M2T: Requirements Model

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture1.png "Metamodelo utilizado")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture2.png "Instância do metamodelo utilizado")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture3.png "Criar projeto Xpand")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture4.png "Escolher o tipo de metamodelo utilizado")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture5.png "Xpand Template")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture61.png "Xpand Extension Languague")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture6.png "Xpand Check Languague")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture7.png "Xpand mwe Workflow")

![Image not found](https://bitbucket.org/mei-isep/edom-18-19-ifp-44/src/master/project-research-topic/images/Picture8.png "Xpand mwe Workflow")

## 4. Bibliografia

 - http://www.idi.ntnu.no/emner/tdt4250/Slides/m2t-xpand1112.pdf
 - https://moodle.isep.ipp.pt/pluginfile.php/243559/mod_resource/content/1/edom07_1.pdf
 - https://learnit.itu.dk/pluginfile.php/121720/course/section/67668/27-30-model-transformations.pdf

 - https://dzone.com/articles/getting-started-code
 - https://help.eclipse.org/oxygen/index.jsp?topic=%2Forg.eclipse.xpand.doc%2Fhelp%2Femf_tutorial_define_metamodel.html&cp=105_0_2

---

## Topics to approach
1. Describe the concern: Model to Text Transformation
2. Describe the approach: Model to Text Transformation using e.g. Epsilon
3. Present a simple case study illustrating how to apply the approach: Requirements Model to Text Transformation using Epsilon);
4. Compare the approach with the approaches lectured in the EDOM course (Acceleo vs Epsilon);
5. Provide a small review of literature references about the approach;

In this folder you should add **all** artifacts developed for the investigative topic of the project.

There is only one folder for this task because it is a team task and, as such, usually there will be no need to create individual folders.

**Note:** If for some reason you need to bypass these guidelines please ask for directions with your teacher and **always** state the exceptions in your commits and issues in bitbucket.
