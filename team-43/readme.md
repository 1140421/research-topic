# EDOM - Project Research Topic

In this folder you should add **all** artifacts developed for the investigative topic of the project.

There is only one folder for this task because it is a team task and, as such, usually there will be no need to create individual folders.

**Note:** If for some reason you need to bypass these guidelines please ask for directions with your teacher and **always** state the exceptions in your commits and issues in bitbucket.


# Índice
1. O que é uma DSL?
2. O que é a Microsoft SDK?
3. Elementos e Ligações
4. Validação do Modelo
5. Bibliografia


# O que é uma DSL?

Domain Specific Language, DSL, é uma notação, geralmente gráfica, em que são definidos elementos, que fazem parte de um modelo, e as suas respetivas ligações. 
Ao contrário das General Purpose Languages (GPL), esta linguagem é dedicada à modelação do domínio de um problema ou técnica particular.

Pelo artigo Domain-Specific Languages: An Annotated Bibliography, Arie van Deursen, Paul Klint e Joost Visser defendem que "A domain-specific language (DSL) is a small, usually declarative, language that offers expressive power focused on a particular problem domain. In many cases, DSL programs are translated to calls to a common subroutine librm'y and the DSL can be viewed as a means to hide the details of that library" (ver referência bibliográfica).

Exemplos de DSL populares:

- SQL;
- HTML;
- UML;
- XML.

Exemplos em que é possível aplicar DSL:

- Seguros de vida;
- Faturação de salários;
- Gestão e manutenção de uma agenda.

# O que é a Microsoft DSL SDK

A Microsoft DSL SDK, tambem conhecido como Modeling SDK (MSDK), é uma ferramenta que permite a criação de um ou vários modelos e integralos num conjunto de ferramentas a utilizar no Visual Studio.
O MSDK acelera o processo de criação do modelo, na forma de uma DSL, facultanto um editor gráfico especializado para defenir um schema ou uma syntax abstrata. Com isso o MSDK juntamente com o Visual Studio gera:

- Uma implementação de um modelo juntamente com a sua respetiva API;
- Explorador em árvore;
- Editor gráfico que permite ao utilizador ver o modelo ou partes do mesmo;
- Métodos de serialização que permitem guardar o modelo em formato XML;
- Facilidade em gerar código e outro artifactos usando templating.


# Modelação - Ferramentas e Propriedades #

Em Microsoft SDK, a DSL é definida pelo ficheiro **DSL Definition**, ao qual pode ser acrescentado manualmente código que personalize a DSL.

Dentro da DSL Definition são utilizadas várias ferramentas para a construção dos  modelos, por exemplo de visualização o do diagrama do modelo, construção do mesmo e exposição dos detalhes dos objetos nele contidos. 

Esta DSL Definition, também denominada por **domain model**, é a representação do modelo no passo de design, sendo que o próprio modelo representa já uma instancia da DSL.


Para criar elementos no domínio utilizam-se as **Domain Classes**, que podem, à semelhança das classes em EMF, ter relações (*links*) entre as mesmas.

A DSL Definition separa a aparência dos modelos utilizando com a utilização de **Shape Classes** ou **Connector Classes**. A informação do modelo é definida pelas **Domain Classes** e **Domain Relationships**.

As **Domain Classes** definem atributos de domínio, por exemplo, uma classe “Livro” poderia ter atributos “titulo”, “autor”, “edição”, etc. As relações entre classes são de um determinado tipo e têm associadas multiplicidade. Em título de exemplo, à classe “Livro” utilizada acima, podemos acrescentar uma relação com uma nova classe “Biblioteca”. Queremos que esta segunda classe tenha uma relação com “Livro” de forma a que seja possível agrupar vários “Livro” dentro de “Biblioteca”. Assim podemos estabelecer uma relação de zero-para-muitos (0..*) na classe “Biblioteca” sobre a classe “Livro”. 

## Multiplicidade

 + 0..* (zero-para-muitos) - Cada instância da Domain Class pode ter multiplas instâncias da relação, ou nenhuma;
 + 0..1 (zero-para-um)     - Cada intância da Domain Class pode ter uma ou nenhuma instância da relação;
 + 1..1 (um)               - Cada intância da Domain Class pode ter uma instância da relação;
 + 1..* (um-para-muitos)   - Cada instância da Domain Class pode ter multiplas instâncias da relação, sendo que cada instância tem que ter sempre pelo menos uma instância da relação.



## Herança

As Domain Classes podem ser definidas por **herança**, no sentido em que estas podem **extender** outros e absorver as suas propriedades. As heranças podem ser utilizadas entre **Relationships**, **Shapes** e **Connectors**, sendo que tem que ser mantida no mesmo grupo, ou seja, um Connector **não pode** herdar propriedades de Shapes ou Relationships.

### Domain Relationships

As **Domain Relationships** definem os *links*/relações entre dois elementos elementos, no entanto, um elemento pode ter várias destas relações com mais elementos, podendo ainda ter várias relações com o mesmo elemento.

Há dois tipos de Relationships:

1.**Embedding Relationships**

	Todos os elementos de um modelo podem são alvos deste tipo de relação (com execeção do elemento raíz). Embedding Relationships representam posse ou composição, sendo que dois elementos relacionados desta forma têm atribuidos um papel de parent (pai, o agregador) e child (filho, o agregado).
	É de notar que este tipo de relação não equivale a herança, dado que o elemento child da relação não herda as propriedades do elemento parent.

2.**Reference Relationships**

	Todos os outros tipos de relação são classificados desta forma. 


#### Roles

Todas as Domain Relationships têm dois Roles, que são o **Source Role** e o **Target Role**.

No exemplo dado anteriormente da "Biblioteca" e "Livro", a relação entre as classes pode ser definida de forma a que o "Livro" tenha o role de target e a "Biblioteca" tenha o role de source.
 

# Validação de Modelos

A semelhança do Eclipse Modeling Framework, EMF, o Microsoft DSL permite que se definam validações nos seus modelos. 
Estas validações podem ser definidas tanto nas classes, como nas relações definidas no modelo desenvolvido. 
Por forma a controlar quando as validações sãso executadas, é possível indicar, através de atributos, as seguintes opções:

- Custom: As validações são corridas quando o metodo ValidateCustom é executado ( este tipo de validações são corridas apenas através de código);
- Load: As validações são corridas quando o modelo é carregado;
- Menu:	As validações são corridas quando o utilizador seleciona no menu;
- Open:	As validações são corridas quando o modelo é aberto;
- Save: As validações são corridas quando o modelo é guardado. 

As validações definidas em classes são propagadas nas suas subclasses, diminuir a replicação de código. 

# Bibliografia

- https://docs.microsoft.com/en-us/visualstudio/modeling/getting-started-with-domain-specific-languages?view=vs-2017
- https://docs.microsoft.com/en-us/visualstudio/modeling/understanding-models-classes-and-relationships?view=vs-2017
- https://docs.microsoft.com/pt-pt/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2017
- https://en.wikipedia.org/wiki/Domain-specific_language
- http://sadiyahameed.pbworks.com/f/deursen+-+DSL+annotated.pdf
- https://msdn.microsoft.com/en-us/data/bb126259(v=vs.85)
